#include <stdio.h>
#include <stdlib.h>

/**
@file clase.h
@brief En esta parte se declara la estructura Clase
@param struct Nombre_Estructura
@
*/
struct Clase{
	float x;
	float y;
	float z;
};
/**
brief Creacion del constructor

*/

void Clase_init(struct Clase *self, float x, float y, float z){
	self -> x =x;
	self -> y =y;
	self -> z =z;
}


/**
asignando una direccion de memoria al constructor
*/
struct Clase* Point_create(float x, float y, float z){
	struct Clase* result = (struct Clase*) malloc(sizeof(struct Clase));
	Clase_init(result, x, y, z);
	return result;
}

